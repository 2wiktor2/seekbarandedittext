package com.example.seekbarandedittext;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    EditText editText1, editText2;

    SeekBar seekBar1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText1 = findViewById(R.id.et_1);
        editText2 = findViewById(R.id.et_2);
        seekBar1 = findViewById(R.id.seekBar_1);


        // отслеживать изменения в EditText
        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int progress = Math.round(Float.parseFloat(s.toString()));
                    seekBar1.setProgress(progress);
                } catch (Exception e) {}

                editText2.setText(editText1.getText().toString());

            }
        });


        // отслеживать изменения в seekBar-e
        seekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                editText1.setText(String.valueOf(progress));
                //Поместить курсор в конец текста в EditText
                editText1.setSelection(editText1.getText().length());

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



    }
}
